# postgisWeb
PostGIS methods exposed as JSON in the cloud

# If you'd like additional methods exposed...
I have only exposed those methods that are used by users. If you'd like additional methods exposed, please [file an issue](https://github.com/hasandiwan/postgisWeb/issues/new) and it will be addressed. You can add code as a comment, but please put each additional stanza as a git diff in a separate comment. 
