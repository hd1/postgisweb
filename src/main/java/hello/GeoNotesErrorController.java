package hello;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GeoNotesErrorController extends AbstractErrorController {
	public GeoNotesErrorController(ErrorAttributes errorAttributes) {
		super(errorAttributes);
	}

	private static Logger logger = LoggerFactory.getLogger(GeoNotesErrorController.class);

	private static String deHump(String word) {
		logger.getClass();
		if (word.isEmpty()) {
			return null;
		}
		String ret = "";
		for (String part : Pattern.compile("(?<=[^\\p{Lu}])(?=[\\p{Lu}])|(?=[\\p{Lu}][^\\p{Lu}])").split(word)) {
			ret = ret + " "+part.toLowerCase();
		}
		return ret.trim();
	}

	public static List<Map<String, String>> getMethodsAnnotatedWith(final Class<?> type,
			final Class<? extends Annotation> annotation) {
		final List<Map<String, String>> methods = new ArrayList<>();
		final List<Method> allMethods_ = new ArrayList<Method>(Arrays.asList(type.getDeclaredMethods()));
		for (int methodIdx = 0; methodIdx != allMethods_.size(); methodIdx++) {
			Method thisMethod = allMethods_.get(methodIdx);
			if (!Modifier.isPublic(thisMethod.getModifiers())) {
				continue;
			}
			Map<String, String> methodMap = new HashMap<>();
			RequestMapping mapped = thisMethod.getAnnotation(RequestMapping.class);
			for (String v : mapped.value()) {
				if (v == null) {
					continue;
				}

				Parameter[] parameters = thisMethod.getParameters();
				for (Parameter param : parameters) {
					if (param.getType().getName() != "java.util.Map") {
						methodMap.put("methodName", thisMethod.getName());
						if (methodMap.containsKey("parameters")) {
							methodMap.put("parameters", methodMap.get("parameters") + ", " + deHump(param.getName()));
						} else {
							methodMap.put("parameters", deHump(param.getName()));
						}
					}
				}
			}
			if (methodMap.containsKey("methodName")) {
				methods.add(methodMap);
			}
		}
		Collections.sort(methods, new Comparator<Map<String, String>>() {
			@Override
			public int compare(Map<String, String> o1, Map<String, String> o2) {
				return o1.get("methodName").compareTo(o2.get("methodName"));
			}

		});
		return methods;
	}

	@RequestMapping(value = { "/error", "/help" }, method = RequestMethod.GET)
	@ResponseBody
	@CrossOrigin
	public ResponseEntity<List<Map<String, String>>> endpoints() {
		List<Map<String, String>> ret = GeoNotesErrorController.getMethodsAnnotatedWith(GreetingController.class,
				Controller.class);
		return new ResponseEntity<>(ret, HttpStatus.NOT_FOUND);
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
}