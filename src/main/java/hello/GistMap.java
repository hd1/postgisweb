package hello;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class GistMap {
	static OkHttpClient client = new OkHttpClient();
	private static Logger LOG = LoggerFactory.getLogger(hello.GistMap.class);

	public static void main(String[] args) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Map<String, String>> fileContent = new HashMap<>();

		for (File testFile :GistMap.getTestFiles()) {
			Map<String, String> contents = new HashMap<>();
			String content = null;
			try {
				content = new String (Files.readAllBytes( Paths.get(testFile.getAbsolutePath())));
			} catch (IOException e) {
				e.printStackTrace();
			}
			contents.put("content", content);
			contents.put("filename", testFile.getName());
			fileContent.put("files", contents);
		}
		String json = null;
		try {
			json = mapper.writeValueAsString(fileContent);
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		RequestBody body = RequestBody.create(MediaType.parse("text/json"), json);
		Request gistRequest = new Request.Builder()
				.addHeader("Authorization", Credentials.basic("hasandiwan", "RjHmSx7cfxPGAM2b")).patch(body)
				.url("https://api.github.com/gists/7efbd34561f10086c41366af5ec9e6e2").build();
		try {
			client.newCall(gistRequest).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		LOG.info("updated gist");

		Request ciRequest = new Request.Builder().url("http://ci.d8u.us:8080/job/postgisweb/build?token=foo").build();
		Response ciResponse = null;
		try {
			ciResponse = client.newCall(ciRequest).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		int status = ciResponse.code();
		LOG.info("test status: "+status);
	}

	private static File[] getTestFiles() {
		File[] ret = null;
		File tempDir = null;
		try {
			tempDir = File.createTempFile("postgisWeb", ".git");
		} catch (IOException e) {
			e.printStackTrace();
			return ret;
		}
		tempDir.mkdir();
		try {
			Git.cloneRepository().setURI("ssh://git@heroku.com/hd1-atlas.git").setDirectory(tempDir).call();
		} catch (GitAPIException e) {
			List<String> stackTrace = Arrays.asList(e.getStackTrace()).stream().map(p -> p.toString()).collect(Collectors.toList());;
			LOG.warn(String.join("\n", stackTrace));
			return ret;
		}
		ret = tempDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				boolean retVal = false;
				try {
					retVal = pathname.getCanonicalPath().contains("src/test/java/hello") && pathname.getName().endsWith(".java");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return retVal;
			}
		}
		);
		return ret;
	}
}
