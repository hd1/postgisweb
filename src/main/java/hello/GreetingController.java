package hello;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.davidmoten.geo.GeoHash;
import com.github.davidmoten.geo.LatLong;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.openlocationcode.OpenLocationCode;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

@RestController
@Controller
@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class })
public class GreetingController {
	private static final Logger LOG = LoggerFactory
			.getLogger(GreetingController.class);

	static OkHttpClient client = new OkHttpClient();

	@PersistenceContext()
	EntityManager emf;

	private static Logger logger = LoggerFactory
			.getLogger(GreetingController.class);

	private String calculateOsm(LatLong coords) {

		String markerLatitude = Double.valueOf(coords.getLat()).toString();
		String markerLongitude = Double.valueOf(coords.getLon()).toString();
		String ret = String.format(
				"https://www.openstreetmap.org/?mlon=%s&mlat=%s",
				markerLongitude, markerLatitude);
		return ret;
	}

	@RequestMapping(value = "/photo", method = RequestMethod.POST)
	public ResponseEntity<Map<String, byte[]>> photo(
			@RequestBody final String geoRaster) {

		final Map<String, byte[]> ret = new TreeMap<>();
		this.emf.unwrap(Session.class).doWork(new Work() {

			@Override
			public void execute(Connection connection) throws SQLException {

				final String SQL = "SELECT ST_AsJpeg(?), ST_AsPNG(?), ST_AsTIFF(?, 'JPEG90')";
				PreparedStatement prepped = connection.prepareStatement(SQL);
				prepped.setString(1, geoRaster);
				prepped.setString(2, geoRaster);
				prepped.setString(3, geoRaster);
				ResultSet res = prepped.executeQuery();
				byte[] jpegBytes = res.getBytes(1);
				byte[] pngBytes = res.getBytes(2);
				byte[] tiffBytes = res.getBytes(3);
				Base64.Encoder encoder = Base64.getUrlEncoder();
				ret.put("tiff", encoder.encode(tiffBytes));
				ret.put("jpeg", encoder.encode(jpegBytes));
				ret.put("png", encoder.encode(pngBytes));
			}
		});
		return new ResponseEntity<>(ret, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/median", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> median(
			@RequestParam(name = "points") final String pts) {

		final Map<String, String> ret = new TreeMap<>();
		this.emf.unwrap(Session.class).doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {

				String SQL = "SELECT ST_X(ST_GeometricMedian('MULTIPOINT(?)'::geometry)), ST_Y(ST_GeometricMedian('MULTIPOINT(?)'::geometry))";
				Statement prepped = connection.createStatement();
				SQL = SQL.replace("?", pts);
				GreetingController.logger.info("About to execute: " + SQL);
				ResultSet res = prepped.executeQuery(SQL);
				res.next();
				ret.put("xCoordinateOfMedian", res.getString(1));
				ret.put("yCoordinateOfMedian", res.getString(2));
			}
		});
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/intersects", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> intersects(
			@RequestParam(name = "fp1") final String firstPoint1,
			@RequestParam(name = "fp2") final String firstPoint2,
			@RequestParam(name = "p") final String point,
			@RequestParam(name = "tol", defaultValue = "0") final String tolerance,
			@RequestParam(name = "speed", required = false) final String velocity) {

		final Map<String, String> ret = new TreeMap<>();
		this.emf.unwrap(Session.class).doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {

				final int X_INDEX = 0;
				final int Y_INDEX = 1;
				String linePoint1[] = firstPoint1.split(",");
				String linePoint2[] = firstPoint2.split(",");
				String pointParts[] = point.split(",");
				String SQL = "SELECT ST_DWithin(ST_MakeLine(ST_Point(?,?),ST_Point(?,?)), ST_Point(?,?), ?), ST_Distance(ST_Point(?,?), ST_Point(?,?))";
				PreparedStatement prepped = connection.prepareStatement(SQL);
				prepped.setDouble(1, Double.valueOf(linePoint1[X_INDEX]));
				prepped.setDouble(2, Double.valueOf(linePoint1[Y_INDEX]));
				prepped.setDouble(3, Double.valueOf(linePoint2[X_INDEX]));
				prepped.setDouble(4, Double.valueOf(linePoint2[Y_INDEX]));
				prepped.setDouble(5, Double.valueOf(pointParts[X_INDEX]));
				prepped.setDouble(6, Double.valueOf(pointParts[Y_INDEX]));
				prepped.setDouble(7, Double.valueOf(tolerance));
				prepped.setDouble(8, Double.valueOf(linePoint1[X_INDEX]));
				prepped.setDouble(9, Double.valueOf(linePoint1[Y_INDEX]));
				prepped.setDouble(10, Double.valueOf(pointParts[X_INDEX]));
				prepped.setDouble(11, Double.valueOf(pointParts[Y_INDEX]));
				ResultSet res = prepped.executeQuery();
				res.next();
				ret.put("line", firstPoint1 + " - " + firstPoint2);
				ret.put("point", point);
				ret.put("intersects?",
						Boolean.valueOf(res.getBoolean(1)).toString());
				if (velocity != null) {
					Double speed = Double.valueOf(velocity);
					Double distanceToCollision = res.getDouble(2);
					ret.put("intersectsAt",
							String.valueOf(distanceToCollision / speed));
				}
			}
		});
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/olc", method = RequestMethod.GET)
	public ResponseEntity<String> locationCode(
			@RequestParam(name = "lat") String latitude,
			@RequestParam(name = "lng") String longitude) {

		final Integer CODE_LENGTH = 8;
		String ret = OpenLocationCode.encode(Double.parseDouble(latitude),
				Double.parseDouble(longitude), CODE_LENGTH);
		return new ResponseEntity<>(ret, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/kmlFromPolygon", method = RequestMethod.POST)
	public ResponseEntity<String> kmlFromPolygon(
			@RequestBody final Map<String, Double> verticesAsCsv) {

		final Map<String, String> map = new TreeMap<>();
		this.emf.unwrap(Session.class).doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {

				final String SQL = "SELECT ST_AsKML(ST_MakePolygon(?), 4326";
				PreparedStatement prepped = connection.prepareStatement(SQL);
				String verticesAsSql = "";
				for (String key : verticesAsCsv.keySet()) {
					if (key.toLowerCase().startsWith("line")) {
						verticesAsSql += String.format("ST_Point(%s)",
								verticesAsCsv.get(key));
					}
				}
				prepped.setString(1, verticesAsSql);
				ResultSet rs = prepped.executeQuery();
				rs.next();
				String xml = HtmlUtils.htmlEscape(rs.getString(1));
				map.put("result", xml);
			}
		});
		return new ResponseEntity<>(map.get("result"), HttpStatus.OK);
	}

	@RequestMapping(value = "/kmlFromPoint", method = RequestMethod.GET)
	public ResponseEntity<String> kmlFromPoint(
			@RequestParam(name = "lat") final String latitude,
			@RequestParam(name = "lng") final String longitude) {

		final Map<String, String> map = new TreeMap<>();
		this.emf.unwrap(Session.class).doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {

				final String SQL = "SELECT ST_AsKML(ST_SetSRID(ST_Point(?, ?), 4326))";
				PreparedStatement prepped = connection.prepareStatement(SQL);
				prepped.setDouble(1, Double.valueOf(longitude));
				prepped.setDouble(2, Double.valueOf(latitude));
				ResultSet rs = prepped.executeQuery();
				rs.next();
				String xml = HtmlUtils.htmlEscape(rs.getString(1));
				map.put("result", xml);
			}
		});
		return new ResponseEntity<>(map.get("result"), HttpStatus.OK);
	}

	@RequestMapping(value = "/geohash", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, String>> geohash(
			@RequestParam(name = "lat") final Double latitude,
			@RequestParam(name = "lng") final Double longitude) {

		final Map<String, String> ret = new TreeMap<>();
		this.emf.unwrap(Session.class).doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {

				final String SQL = "SELECT ST_GeoHash(ST_SetSRID(ST_MakePoint(?,?), 4326))";
				PreparedStatement prepped = connection.prepareStatement(SQL);
				prepped.setDouble(1, longitude);
				prepped.setDouble(2, latitude);
				ResultSet rs = prepped.executeQuery();
				rs.next();
				ret.put("geohash", rs.getString(1));
			}
		});
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/resolveGeohash", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, String>> decodeGeohash(
			@RequestParam(name = "q") String incomingGeohash) {

		final TreeMap<String, String> ret = new TreeMap<>();
		GreetingController.logger
		.info("Incoming geohash is " + incomingGeohash);
		LatLong coords = GeoHash.decodeHash(incomingGeohash);
		ret.put("latitude", String.valueOf(coords.getLat()));
		ret.put("longitude", String.valueOf(coords.getLon()));
		ret.put("openstreetMap", this.calculateOsm(coords));
		return new ResponseEntity<>((ret), HttpStatus.OK);
	}

	@RequestMapping(value = "/geocode", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, String>> geocode(
			@RequestParam Map<String, String> q) {

		Map<String, String> ret = new TreeMap<>();
		if (q.containsKey("ip")) {
			HttpUrl url = new HttpUrl.Builder().scheme("http")
					.host("api.ipstack.com").encodedPath("/" + q.get("ip"))
					.addQueryParameter("access_key",
							"75ede55efce8fdc97bf8d875460f0481")
					.addQueryParameter("format", "1").build();
			Request urlRequest = new Request.Builder().url(url).build();
			String received = null;
			try {
				received = GreetingController.client.newCall(urlRequest)
						.execute().body().string();
			} catch (IOException e) {
				e.printStackTrace();
			}
			JsonElement element_ = new JsonParser().parse(received);
			JsonObject element = element_.getAsJsonObject();
			ret.put("city", element.get("city").getAsString());
			ret.put("country", element.get("country_name").getAsString());
		} else { // q.containsKey("q")
			String url = HttpUrl
					.parse("https://nominatim.openstreetmap.org/search")
					.newBuilder()
					.addQueryParameter("format", HtmlUtils.htmlEscape("json"))
					.addQueryParameter("q", HtmlUtils.htmlEscape(q.get("q")))
					.toString();
			Request request = new Request.Builder().url(url).build();
			String finalReturn = null;
			Response response = null;
			try {
				response = GreetingController.client.newCall(request).execute();
			} catch (IOException e1) {
				e1.printStackTrace();
				return new ResponseEntity<>(
						Collections.singletonMap("error", "nominatim is down"),
						HttpStatus.SERVICE_UNAVAILABLE);
			}
			String rawJson = null;
			try {
				rawJson = response.body().string();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			JsonElement jsonTree = new JsonParser().parse(rawJson);
			try {
				JsonObject obj = jsonTree.getAsJsonArray().get(0)
						.getAsJsonObject();
				ret.put("displayName", obj.get("display_name").getAsString());
				ret.put("latitude", Double.valueOf(obj.get("lat").getAsDouble())
						.toString());
				ret.put("longitude", Double
						.valueOf(obj.get("lon").getAsDouble()).toString());
				ret.put("openstreetmapUrl",
						String.format(
								"https://www.openstreetmap.org/#map=19/%s/%s",
								ret.get("latitude"), ret.get("longitude")));
				ret.put("query", q.get("q"));
			} catch (java.lang.IndexOutOfBoundsException e) {
				finalReturn = "upstream timeout";
				ret.put("error", finalReturn);
				return new ResponseEntity<>(ret, HttpStatus.REQUEST_TIMEOUT);
			}
		}
		return new ResponseEntity<>(ret, HttpStatus.FOUND);

	}

	@RequestMapping(value = "/{longitude}/{latitude}/isRound", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Map<String, String>> pointInsideCircle(
			@PathVariable("longitude") final Float pointLongitude,
			@PathVariable("latitude") final Float pointLatitude,
			@RequestBody final Map<String, Float> circle) {

		try {
			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
			GreetingController.logger.info(writer.writeValueAsString(circle));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new ResponseEntity<>(
					Collections.singletonMap("error", e.getLocalizedMessage()),
					HttpStatus.PRECONDITION_FAILED);
		}
		final Map<String, String> retVal = new TreeMap<>();
		this.emf.unwrap(Session.class).doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {

				PreparedStatement prepped = connection.prepareStatement(
						"SELECT ST_PointInsideCircle(ST_Point(?, ?), ?, ?, ?)");
				prepped.setFloat(1, pointLongitude);
				prepped.setFloat(2, pointLatitude);
				prepped.setFloat(3, circle.get("center.x"));
				prepped.setFloat(4, circle.get("center.y"));
				prepped.setFloat(5, circle.get("radius"));
				ResultSet res = prepped.executeQuery();
				res.next();
				retVal.put("inCircle", String.valueOf(res.getBoolean(1)));
			}
		});
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}

	@RequestMapping(value = "/{longitude}/{latitude}/bounds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Serializable>> boundsCheck(
			@PathVariable("longitude") final Float pointLongitude,
			@PathVariable("latitude") final Float pointLatitude,
			@RequestBody final Map<String, Float> boundingBoxParams) {

		final Map<String, Serializable> retVal = new TreeMap<>();

		this.emf.unwrap(Session.class).doWork(new Work() {
			@Override
			public void execute(Connection arg0) throws SQLException {

				final String sql = "SELECT ST_Within(ST_Point(?,?), ST_MakeEnvelope(?, ?, ?, ?))";
				PreparedStatement prep = arg0.prepareStatement(sql);
				prep.setFloat(1, pointLongitude);
				prep.setFloat(2, pointLatitude);
				prep.setFloat(3, boundingBoxParams.get("minX"));
				prep.setFloat(4, boundingBoxParams.get("minY"));
				prep.setFloat(5, boundingBoxParams.get("maxX"));
				prep.setFloat(6, boundingBoxParams.get("maxY"));
				ResultSet rs = prep.executeQuery();
				rs.next();
				retVal.put("inBoundingBox", rs.getBoolean(1));
			}
		});
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> distanceAndBearing(
			@RequestParam(value = "olng") final Float ourLongitude,
			@RequestParam(value = "olat") final Float ourLatitude,
			@RequestParam(value = "lat") final Float markerLatitude,
			@RequestParam(value = "lng") final Float markerLongitude) {

		if ((Math.abs(ourLongitude) > 180) || (Math.abs(ourLatitude) > 90)
				|| (Math.abs(markerLongitude) > 180)
				|| (Math.abs(markerLatitude) > 90)) {
			return new ResponseEntity<>(Collections.singletonMap("error",
					"One of your values is out of range -- latitudes must be from -90 to 90; longitudes must be from -180 to 180"),
					HttpStatus.PRECONDITION_FAILED);
		}
		try {
			final String sql = "SELECT "
					+ "ST_X(ST_AsText(ST_Centroid(ST_Collect(ST_Point(?,?), ST_Point(?,?))))) || ',' || ST_Y(ST_AsText(ST_Centroid(ST_Collect(ST_Point(?,?), ST_Point(?,?))))) as center, "
					+ "COALESCE(0,degrees(ST_Azimuth(ST_Point(?,?), ST_Point(?,?)))) as bearing, "
					+ "ST_DistanceSphere(ST_Point(?,?), ST_Point(?,?)) as distance, "
					+ "COALESCE(0, ST_Azimuth(ST_Point(?,?), ST_Point(?,?))) as bearingInRadians;";
			final TreeMap<String, String> ret = new TreeMap<>();
			this.emf.unwrap(Session.class).doWork(new Work() {

				@Override
				public void execute(Connection arg0) throws SQLException {

					PreparedStatement prepped = arg0.prepareStatement(sql);
					prepped.setFloat(1, ourLongitude);
					prepped.setFloat(2, ourLatitude);
					prepped.setFloat(3, markerLongitude);
					prepped.setFloat(4, markerLatitude);
					prepped.setFloat(5, ourLongitude);
					prepped.setFloat(6, ourLatitude);
					prepped.setFloat(7, markerLongitude);
					prepped.setFloat(8, markerLatitude);
					prepped.setFloat(9, ourLongitude);
					prepped.setFloat(10, ourLatitude);
					prepped.setFloat(11, markerLongitude);
					prepped.setFloat(12, markerLatitude);
					prepped.setFloat(13, ourLongitude);
					prepped.setFloat(14, ourLatitude);
					prepped.setFloat(15, markerLongitude);
					prepped.setFloat(16, markerLatitude);
					prepped.setFloat(17, ourLongitude);
					prepped.setFloat(18, ourLatitude);
					prepped.setFloat(19, markerLongitude);
					prepped.setFloat(20, markerLatitude);
					GreetingController.logger
					.info(prepped + " is about to be executed");
					ResultSet rs = prepped.executeQuery();
					rs.next();
					ret.put("center", rs.getString(1));
					ret.put("bearing", rs.getString(2));
					ret.put("distance", rs.getString(3));
					ret.put("bearinginradians", rs.getString(4));
				}
			});

			return new ResponseEntity<>(ret, HttpStatus.OK);
		} catch (DataAccessException e) {
			return new ResponseEntity<>(
					Collections.singletonMap("error", e.toString()),
					HttpStatus.PRECONDITION_FAILED);
		}
	}

	// 0,0
	@RequestMapping(value = "/collision", method = RequestMethod.GET)
	public ResponseEntity<List<Map<String, String>>> collisionDetection(
			@RequestParam(name = "loc") String loc) {
		List<Map<String, String>> ret = new ArrayList<>();
		ICsvListReader listReader = new CsvListReader(new StringReader(loc),
				CsvPreference.STANDARD_PREFERENCE);
		try {
			listReader.getHeader(true);
		} catch (IOException e) {
			GreetingController.LOG.error(e.getMessage(), e);
		} // skip the header (can't be used with
		// CsvListReader)
		String latitude = listReader.get(1);
		String longitude = listReader.get(2);
		this.emf.unwrap(Session.class).doWork(new Work() {

			@Override
			public void execute(Connection arg0) throws SQLException {

				ResultSet res = arg0
						.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
								ResultSet.CONCUR_READ_ONLY)
						.executeQuery(
								"select st_x(location) as lng, st_y(location) as lat from collision_store");
				PreparedStatement prepped = arg0.prepareStatement("select st_distance(ST_SetSRID(st_point(?, ?), 4326), st_setsrid(st_point(?, ?), 4326)) as d, st_azimuth(st_setrsid(st_point(?,?), 4326), st_setrsid(st_point(?,?), 4326)) as a");
				prepped.setString(1, longitude);
				prepped.setString(2, latitude);
				prepped.setString(5, longitude);
				prepped.setString(6, latitude);
				for (res.first(); res.isAfterLast() == false; res.next()) {
					Double thisLongitude = res.getDouble(1);
					Double thisLatitude = res.getDouble(2);
					prepped.setDouble(3, thisLongitude);
					prepped.setDouble(4, thisLatitude);
					prepped.setDouble(7, thisLongitude);
					prepped.setDouble(8, thisLatitude);
					ResultSet mapToAdd = prepped.executeQuery();
					Map<String, String> result = new HashMap<>();
					result.put("distance", mapToAdd.getString(1));
					result.put("azimuth", mapToAdd.getString(2));
					ret.add(result);
				}
			}

		});
		try {
			listReader.close();
		} catch (IOException e) {
			GreetingController.LOG.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}
}
