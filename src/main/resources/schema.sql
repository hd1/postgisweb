CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE TABLE known_positions(position_id integer primary key, satellite_name uuid, timestamp date, latitude double, longitude double);