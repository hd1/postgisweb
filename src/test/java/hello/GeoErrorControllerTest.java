package hello;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

// FIXME restore Heroku deployment

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
@AutoConfigureMockMvc
public class GeoErrorControllerTest {
	@Autowired
	private MockMvc mockMvc;
	final String METHOD_RESPONSE = "[{\"methodName\":\"boundsCheck\",\"parameters\":\"point longitude, point latitude\"},{\"methodName\":\"decodeGeohash\",\"parameters\":\"incoming geohash\"},{\"methodName\":\"distanceAndBearing\",\"parameters\":\"our longitude, our latitude, marker latitude, marker longitude\"},{\"methodName\":\"geohash\",\"parameters\":\"latitude, longitude\"},{\"methodName\":\"intersects\",\"parameters\":\"first point1, first point2, point, tolerance, velocity\"},{\"methodName\":\"kmlFromPoint\",\"parameters\":\"latitude, longitude\"},{\"methodName\":\"locationCode\",\"parameters\":\"latitude, longitude\"},{\"methodName\":\"median\",\"parameters\":\"pts\"},{\"methodName\":\"photo\",\"parameters\":\"geo raster\"},{\"methodName\":\"pointInsideCircle\",\"parameters\":\"point longitude, point latitude\"}]";
	final OkHttpClient client = new OkHttpClient();
	
	@Test
	public void remoteUrl() throws Exception {
		HttpUrl.Builder httpBuilder = HttpUrl.parse("https://hd1-atlas.herokuapp.com/geocode").newBuilder();
		httpBuilder.addQueryParameter("q", "10 Downing Street, London, England");
		Request request = new Request.Builder().get().url(httpBuilder.build()).build();
		Response resp = client.newCall(request).execute();
		Assert.assertNotEquals(201, resp.code());
	}
	@Test
	public void helpMethodListContainsAllSupportedMethods() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/help").header("Content-Type", "application/json")).andReturn();
		String content = result.getResponse().getContentAsString();
		try {
			new ObjectMapper().readValue(content, List.class);
			Assert.assertTrue("error returns list", true);
		} catch (JsonMappingException e) {
			Assert.fail("/error doesn't return list");
		}
		Assert.assertEquals(METHOD_RESPONSE, content);
	}

	@Test
	public void errorMethodListIsAList() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/error").header("Content-Type", "application/json")).andReturn();
		String content = result.getResponse().getContentAsString();
		try {
			new ObjectMapper().readValue(content, List.class);
			Assert.assertTrue("error returns list", true);
		} catch (JsonMappingException e) {
			Assert.fail("/error doesn't return list");
		}
	}
}
