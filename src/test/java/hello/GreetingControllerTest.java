package hello;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
@AutoConfigureMockMvc
// https://hd1-atlas.herokuapp.com/
public class GreetingControllerTest {

	private static final int UPSTREAM_ERROR_STATUS = 408;

	private static final Logger LOG = LoggerFactory
			.getLogger(GreetingController.class);

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public SingleConnectionDataSource getDataSource() {

		SingleConnectionDataSource ds = new SingleConnectionDataSource();
		ds.setDriverClassName("org.hsqldb.jdbcDriver");
		ds.setUrl("jdbc:h2:mem:testdb");
		ds.setUsername("sa");
		ds.setAutoCommit(false);
		return ds;
	}

	@BeforeClass
	public void setUp() {

		Connection conn = null;
		try {
			conn = this.getDataSource().getConnection();
			conn.createStatement().execute(
					"CREATE TABLE junk_locations (id integer, description text, latitude text, longitude text, altitude text)");
			conn.commit();
		} catch (SQLException e) {
			GreetingControllerTest.LOG.error(e.getMessage(), e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					GreetingControllerTest.LOG.error(e.getMessage(), e);
				}
			}
		}
	}

	@AfterClass
	public void tearDown() {

		Connection conn = null;
		try {
			conn = this.getDataSource().getConnection();
			conn.createStatement().execute("DROP TABLE junk_locations");
		} catch (SQLException e) {
			GreetingControllerTest.LOG.error(e.getMessage(), e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					GreetingControllerTest.LOG.error(e.getMessage(), e);
				}
			}
		}
	}

	@Test
	public void median() throws Exception {

		MvcResult response = this.mockMvc
				.perform(
						MockMvcRequestBuilders.get("/median?points=" + HtmlUtils
								.htmlEscape("(0 0), (1 1), (2 2), (200 200)")))
				.andReturn();
		Map<String, String> map = new ObjectMapper().readValue(
				response.getResponse().getContentAsString(),
				new TypeReference<Map<String, String>>() {
				});
		Assert.assertTrue(map.get("xCoordinateOfMedian").startsWith("1.97"));
		Assert.assertTrue(map.get("yCoordinateOfMedian").startsWith("1.97"));
	}

	@Test
	public void intersectsWithSpeed() throws Exception {

		MvcResult response = this.mockMvc
				.perform(MockMvcRequestBuilders
						.get("/intersects?fp1=0,0&fp2=2,2&p=1,1&speed=2"))
				.andReturn();
		Map<String, String> map = new ObjectMapper().readValue(
				response.getResponse().getContentAsString(),
				new TypeReference<Map<String, String>>() {
				});
		Assert.assertEquals("0.7071067811865476", map.get("intersectsAt"));
	}

	@Test
	public void intersectionWithTolerance() throws Exception {

		MvcResult response = this.mockMvc
				.perform(MockMvcRequestBuilders
						.get("/intersects?fp1=0,0&fp2=2,0&p=1,1&tol=2"))
				.andReturn();
		Map<String, String> map = new ObjectMapper().readValue(
				response.getResponse().getContentAsString(),
				new TypeReference<Map<String, String>>() {
				});
		Assert.assertEquals("true", map.get("intersects?"));

		response = this.mockMvc
				.perform(MockMvcRequestBuilders
						.get("/intersects?fp1=0,0&fp2=2,0&p=1,14&tol=1"))
				.andReturn();
		map = new ObjectMapper().readValue(
				response.getResponse().getContentAsString(),
				new TypeReference<Map<String, String>>() {
				});
		Assert.assertEquals("false", map.get("intersects?"));
	}

	@Test
	public void collisionEndpointReturnsListOfDistancesAndBearingsOrderedByClosestPass()
			throws Exception {

		this.jdbcTemplate.update(
				"insert into collision_store (location) values (?, ?)", 1, 1);
		Map<String, List<Map<String, String>>> responseMap = new ObjectMapper()
				.readValue(
						this.mockMvc
						.perform(MockMvcRequestBuilders
								.get("/collisions?loc=0,0"))
						.andReturn().getResponse().getContentAsString(),
						new TypeReference<Map<String, List<Map<String, String>>>>() {
						});
		List<Map<String, String>> collisionDistancesAndBearings = responseMap
				.get("collisionProbabilities");
		for (Map<String, String> obj : collisionDistancesAndBearings) {
			Assert.assertTrue(obj.containsKey("distance"));
			Assert.assertTrue(obj.containsKey("azimuth"));
			Assert.assertTrue(obj.containsKey("velocity"));
			try {
				Double.parseDouble(obj.get("distance"));
			} catch (Exception e) {
				Assert.fail("distance metric is non-numeric");
			}
			try {
				Double.parseDouble(obj.get("azimuth"));
			} catch (Exception e) {
				Assert.fail("azimuth metric is non-numeric");
			}
			try {
				Double.parseDouble(obj.get("velocity"));
			} catch (Exception e) {
				Assert.fail("velocity is non-numeric");
			}
		}
	}

	@Test
	public void intersection() throws Exception {

		MvcResult response = this.mockMvc.perform(
				MockMvcRequestBuilders.get("/intersects?fp1=0,0&fp2=2,0&p=1,0"))
				.andReturn();
		Map<String, String> map = new ObjectMapper().readValue(
				response.getResponse().getContentAsString(),
				new TypeReference<Map<String, String>>() {
				});
		Assert.assertEquals("true", map.get("intersects?"));
	}

	@Test
	public void geocodingIpReturnsCityAndCountry() throws Exception {

		MvcResult response = this.mockMvc
				.perform(MockMvcRequestBuilders
						.get("/geocode?ip="
								+ URLEncoder.encode("128.113.1.5", "UTF-8"))
						.header("Content-Type", "application/json"))
				.andReturn();
		Map<String, String> ret = new ObjectMapper().readValue(
				response.getResponse().getContentAsString(),
				new TypeReference<Map<String, String>>() {
				});
		Assert.assertTrue(ret.containsKey("city"));
		Assert.assertTrue(ret.containsKey("country"));
	}

	@Test
	public void geocodingStreetAddressReturnsLatitudeAndLongitude()
			throws Exception {

		MvcResult response = this.mockMvc
				.perform(MockMvcRequestBuilders
						.get("/geocode?q=10+downing+street,+london"))
				.andReturn();
		Map<String, String> ret = new ObjectMapper().readValue(
				response.getResponse().getContentAsString(),
				new TypeReference<Map<String, String>>() {
				});
		LoggerFactory.getLogger("hello.GreetingControllerTest")
		.info(response.getResponse().getContentAsString());
		if (response.getResponse()
				.getStatus() == GreetingControllerTest.UPSTREAM_ERROR_STATUS) {
			Assert.assertTrue(ret.containsKey("error"));
			Assert.assertEquals(ret.get("error"), "upstream timeout");
		} else {
			Assert.assertTrue(ret.containsKey("displayName"));
			Assert.assertTrue(ret.containsKey("latitude"));
			Assert.assertEquals(
					"\"https://www.openstreetmap.org/#map=19/51.50344025/-0.127708209585621",
					ret.get("openstreetmapUrl"));
			Assert.assertTrue(ret.get("latitude").startsWith("51"));
			Assert.assertTrue(ret.containsKey("longitude"));
			Assert.assertTrue(ret.get("longitude").startsWith("-0.12"));
		}
	}

	@Test
	public void isPointInCircle() throws Exception {

		java.util.Map<String, Float> params = new HashMap<>();
		params.put("center.x", (float) 1);
		params.put("center.y", (float) -1);
		params.put("radius", (float) 3);
		this.mockMvc
		.perform(MockMvcRequestBuilders.post("/0/0/isRound")
				.content(new ObjectMapper().writeValueAsString(params))
				.header("Content-Type", "application/json"))
		.andExpect(MockMvcResultMatchers.jsonPath("$.inCircle")
				.value("true"));
		this.mockMvc
		.perform(MockMvcRequestBuilders.post("/8/8/isRound")
				.content(new ObjectMapper().writeValueAsString(params))
				.header("Content-Type", "application/json"))
		.andExpect(MockMvcResultMatchers.jsonPath("$.inCircle")
				.value("false"));
	}

	@Test
	public void outOfRangeLatitudeOrLongitudeReturns422WithAppropriateErrorMessage()
			throws Exception {

		this.mockMvc
		.perform(MockMvcRequestBuilders
				.get("/info?olat=0&olng=0&lat=181&lng=0"))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().is(412))
		.andExpect(MockMvcResultMatchers.jsonPath("$.error").value(
				"One of your values is out of range -- latitudes must be from -90 to 90; longitudes must be from -180 to 180"));
	}

	@Test
	public void zeroBearingDoesNotReturnNull() throws Exception {

		this.mockMvc
		.perform(MockMvcRequestBuilders
				.get("/info?olat=0&olng=0&lat=0&lng=0"))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().is(200))
		.andExpect(MockMvcResultMatchers.jsonPath("$.bearing")
				.isNotEmpty());
	}

	@Test
	public void bearingCalculationWorks() throws Exception {

		this.mockMvc
		.perform(MockMvcRequestBuilders
				.get("/info?olng=0&olat=0&lat=90&lng=0"))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().is(200))
		.andExpect(
				MockMvcResultMatchers.jsonPath("$.bearing").value(0.0))
		.andExpect(MockMvcResultMatchers.jsonPath("$.bearinginradians",
				Matchers.equalTo("0")));
	}

	@Test
	public void geohash() throws Exception {

		this.mockMvc
		.perform(MockMvcRequestBuilders.get("/geohash?lat=48&lng=-126"))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.jsonPath("$.geohash")
				.value("c0w3hf1s70w3hf1s70w3"));
	}

	@Test
	public void kmlFromPointIsEscaped() throws Exception {

		String expected = "&lt;Point&gt;&lt;coordinates&gt;0,0&lt;/coordinates&gt;&lt;/Point&gt;";
		String actual = this.mockMvc
				.perform(
						MockMvcRequestBuilders.get("/kmlFromPoint?lat=0&lng=0"))
				.andReturn().getResponse().getContentAsString();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void reverseGeohash() throws Exception {

		String response = this.mockMvc
				.perform(MockMvcRequestBuilders
						.get("/resolveGeohash?q=c0w3hf1s70w3hf1s70w3"))
				.andReturn().getResponse().getContentAsString();
		Map<String, String> geohashResponse = new ObjectMapper()
				.readValue(response, new TypeReference<Map<String, String>>() {
				});
		Assert.assertTrue(geohashResponse.get("latitude").startsWith("48"));
		Assert.assertTrue(geohashResponse.get("longitude").startsWith("-126"));
		Assert.assertEquals(geohashResponse.get("openstreetMap"),
				String.format("https://www.openstreetmap.org/?mlon=%s&mlat=%s",
						geohashResponse.get("longitude"),
						geohashResponse.get("latitude")));
	}

	@Test
	public void pointsInEnvelope() throws Exception {

		Map<String, Float> params = new HashMap<>();
		params.put("minX", (float) 9.0);
		params.put("maxX", (float) 12.0);
		params.put("minY", (float) 9.0);
		params.put("maxY", (float) 12.0);
		this.mockMvc
		.perform(MockMvcRequestBuilders.post("/10.5/11/bounds")
				.content(new ObjectMapper().writeValueAsString(params))
				.header("Content-Type", "application/json"))
		.andExpect(MockMvcResultMatchers.jsonPath("$.inBoundingBox")
				.value(Boolean.TRUE));
	}
}
